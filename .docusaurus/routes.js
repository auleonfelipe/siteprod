import React from 'react';
import ComponentCreator from '@docusaurus/ComponentCreator';

export default [
  {
    path: '/siteprod/blog',
    component: ComponentCreator('/siteprod/blog', 'fcf'),
    exact: true
  },
  {
    path: '/siteprod/blog/archive',
    component: ComponentCreator('/siteprod/blog/archive', '54d'),
    exact: true
  },
  {
    path: '/siteprod/blog/first-blog-post',
    component: ComponentCreator('/siteprod/blog/first-blog-post', 'eb6'),
    exact: true
  },
  {
    path: '/siteprod/blog/long-blog-post',
    component: ComponentCreator('/siteprod/blog/long-blog-post', '5a6'),
    exact: true
  },
  {
    path: '/siteprod/blog/mdx-blog-post',
    component: ComponentCreator('/siteprod/blog/mdx-blog-post', 'b34'),
    exact: true
  },
  {
    path: '/siteprod/blog/tags',
    component: ComponentCreator('/siteprod/blog/tags', '661'),
    exact: true
  },
  {
    path: '/siteprod/blog/tags/docusaurus',
    component: ComponentCreator('/siteprod/blog/tags/docusaurus', '099'),
    exact: true
  },
  {
    path: '/siteprod/blog/tags/facebook',
    component: ComponentCreator('/siteprod/blog/tags/facebook', '710'),
    exact: true
  },
  {
    path: '/siteprod/blog/tags/hello',
    component: ComponentCreator('/siteprod/blog/tags/hello', '94e'),
    exact: true
  },
  {
    path: '/siteprod/blog/tags/hola',
    component: ComponentCreator('/siteprod/blog/tags/hola', '187'),
    exact: true
  },
  {
    path: '/siteprod/blog/welcome',
    component: ComponentCreator('/siteprod/blog/welcome', '1c3'),
    exact: true
  },
  {
    path: '/siteprod/markdown-page',
    component: ComponentCreator('/siteprod/markdown-page', '475'),
    exact: true
  },
  {
    path: '/siteprod/docs',
    component: ComponentCreator('/siteprod/docs', '73c'),
    routes: [
      {
        path: '/siteprod/docs',
        component: ComponentCreator('/siteprod/docs', 'ef2'),
        routes: [
          {
            path: '/siteprod/docs',
            component: ComponentCreator('/siteprod/docs', '1c2'),
            routes: [
              {
                path: '/siteprod/docs/category/tutorial---basics',
                component: ComponentCreator('/siteprod/docs/category/tutorial---basics', 'ccd'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/siteprod/docs/category/tutorial---extras',
                component: ComponentCreator('/siteprod/docs/category/tutorial---extras', '925'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/siteprod/docs/intro',
                component: ComponentCreator('/siteprod/docs/intro', 'db8'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/siteprod/docs/tutorial-basics/congratulations',
                component: ComponentCreator('/siteprod/docs/tutorial-basics/congratulations', '2ee'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/siteprod/docs/tutorial-basics/create-a-blog-post',
                component: ComponentCreator('/siteprod/docs/tutorial-basics/create-a-blog-post', 'ba5'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/siteprod/docs/tutorial-basics/create-a-document',
                component: ComponentCreator('/siteprod/docs/tutorial-basics/create-a-document', '435'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/siteprod/docs/tutorial-basics/create-a-page',
                component: ComponentCreator('/siteprod/docs/tutorial-basics/create-a-page', 'f08'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/siteprod/docs/tutorial-basics/deploy-your-site',
                component: ComponentCreator('/siteprod/docs/tutorial-basics/deploy-your-site', '764'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/siteprod/docs/tutorial-basics/markdown-features',
                component: ComponentCreator('/siteprod/docs/tutorial-basics/markdown-features', '023'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/siteprod/docs/tutorial-extras/manage-docs-versions',
                component: ComponentCreator('/siteprod/docs/tutorial-extras/manage-docs-versions', 'd52'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/siteprod/docs/tutorial-extras/translate-your-site',
                component: ComponentCreator('/siteprod/docs/tutorial-extras/translate-your-site', '4f9'),
                exact: true,
                sidebar: "tutorialSidebar"
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '/siteprod/',
    component: ComponentCreator('/siteprod/', 'e7a'),
    exact: true
  },
  {
    path: '*',
    component: ComponentCreator('*'),
  },
];
